import axios from 'axios';
import { message } from 'antd';
// todo axios 二次封装
const http = axios.create({
  // baseURL: 'https://some-domain.com/api/',
  timeout: 10000, // 若是 ？ 秒没有返回数据  就算了
  // headers: {'X-Custom-Header': 'foobar'}
});

// 添加请求拦截器
http.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
http.interceptors.response.use(
  function ({ data }) {
    // 数据请求成功  接口返回成功状态的时候执行
    return Promise.resolve({
      code: 1, // 成功  返回  1
      ...data,
    });
  },
  function (error) {
    // 数据请求失败  对响应错误做点什么
    let msg = '';
    if (error.code === 'ECONNABORTED') {
      msg = '网络请求超时，请刷新重试~';
      message.error(msg); // 友好提示
      return Promise.reject({
        code: 0, // 失败  返回  0
        msg,
      });
    }
  }
);

export default http;
