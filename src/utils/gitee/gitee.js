const configDate = {
  client_id: '150a9fd9eac00be955c20d29a37261f0e29c27dc9635cf13691873f8555ec4e1',
  redirect_uri: 'http://localhost:3000/active',
};

export const Giteedata = () => {
  window.location.href = `https://gitee.com/oauth/authorize?client_id=${configDate.client_id}&redirect_uri=${configDate.redirect_uri}&response_type=code
  `;
};

export const giteCode = () => {
  return window.location.search.slice(1).split('=')[1];
};
