const data = {
  myMessage: 'hello',
  active: 'active',
  pigeonhole: 'Pigeonhole',
  knowledge: 'Knowledge',
  message: 'Message',
  about: 'About',
};

export default data;
