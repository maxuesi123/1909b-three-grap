/* eslint-disable no-console */
const context = require.context('../store/module', false, /\.js$/);
console.log(context);

const data = context.keys().reduce((def, cur) => {
  // console.log(context(cur), cur, '^^^^^^^^^^^'); //'./numStore.js'
  let key = cur.match(/^\.\/(\w+)\.js$/)[1];
  def[key] = context(cur).default;
  return def;
}, {});

// console.log(data, '****');

export default data;

// {
//     titleStore:context(cur).default,
//     numStore:context(cur).default,
// }
