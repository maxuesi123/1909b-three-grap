// @ts-ignore
import axios from '@/utils/axios';
//归档页面数据
export const getGuiDang = () => axios.get('/api/article/archives');
//文章页面搜索数据
export const getSeach = () => axios.get('/api/article/archives');
//知识小册数据
export const getKnow = () => axios.get('/api/Knowledge');
//文章分类 详情
export const getKnow_detail = () => axios.get('/api/category');
//推荐阅读 详情
export const getReact_detail = () => axios.get('/api/article/recommend');
export const floorData = () => axios.get('/api/article/archives');

// export const getactive = () => axios.get('api/Knowledge');
export const Readdata = () => axios.get('/api/article');
