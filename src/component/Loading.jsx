// @ts-nocheck
import React, { Component } from 'react';
import Indexmodule from '../views/style/index.module.less';
import { withRouter } from 'react-router-dom';
class Loading extends Component {
  handeLoading = () => {
    this.props.history.replace('/active');
  };
  render() {
    return (
      <div className={Indexmodule.loading}>
        <img
          height="36"
          src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-logo.png"
          alt="logo"
          onClick={this.handeLoading}
        />
      </div>
    );
  }
}

export default withRouter(Loading);
