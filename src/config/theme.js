//切换主题样式

export const light = {
  '@bg-color': '#e7eaee',
  '@text-color': '#000',
};

export const dark = {
  '@bg-color': '#2b2b2c',
  '@text-color': '#fff',
};
