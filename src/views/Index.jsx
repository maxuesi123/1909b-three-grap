import React, { Component } from 'react';
import RouterView from '../router/RouterView';
import Loading from '../component/Loading';
import { Icon, Button, Form, Field } from 'react-vant';
import Indexmodule from '@/views/style/index.module.less';
import { Indexroutes } from '@/router/routerConfig';
import { Menu, Modal, Input, Divider } from 'antd';
import { dark, light } from '@/config/theme.js';
import { getSeach } from '@/api/GuiDang';
import { Giteedata } from '@/utils/gitee/gitee.js';
import img1 from '../utils/img/img1.png';
import img2 from '../utils/img/img2.png';
// import img3 from '../utils/img/img3.png';
import { FormattedMessage } from 'react-intl';
import { NavLink } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
@inject('titlestore')
@observer

// import { Indexroutes } from '../router/routerConfig';
export class Index extends Component {
  state = {
    flag: true,
    isModalVisible: false,
    list: [],
    SearchList: [],
    searchflag: false,
    searchval: '',
    LoginisModalVisible: false,
    form: '',
    headrightFlag: true,
  };
  componentDidMount() {
    const getSearchData = async () => {
      const res = await getSeach();
      this.setState({
        list: res.data[2022].May,
        SearchList: res.data[2022].May,
      });
    };
    getSearchData();
  }

  //主题切换
  handeClickFlag() {
    const { flag } = this.state;
    this.setState(
      {
        flag: !flag,
      },
      () => {
        //改变主题颜色

        // @ts-ignore
        window.less.modifyVars(!flag ? dark : light);
      }
    );
  }
  //模糊搜索
  handeClickSearch() {
    this.setState({ isModalVisible: true });
  }
  handleCancel() {
    this.setState({ isModalVisible: false });
  }

  //改变输入的时候显示底下的数据
  handeSearchInput(e) {
    this.setState({
      searchflag: !this.state.searchflag,
      searchval: e.target.value,
    });
    this.state.list = this.state.SearchList.filter((item, index) => {
      return item.title.includes(e.target.value);
    });
  }
  handeSearchSou(searchval) {
    this.state.list = this.state.SearchList.filter((item, index) => {
      return item.title.includes(searchval);
    });
    this.setState({ searchflag: !this.state.searchflag });
  }
  //登录显示弹框
  handeLogin() {
    this.setState({ LoginisModalVisible: true });
  }
  handleCancels() {
    this.setState({ LoginisModalVisible: false });
  }
  //登录
  onFinish(e) {
    console.log(e);
  }
  //第三方登录
  handeGitee_Login() {
    Giteedata();
  }
  //切换中英文
  handSelect() {
    this.props.titlestore.changeLange();
  }
  //响应是布局
  handX() {
    this.setState({ headrightFlag: !this.state.headrightFlag });
  }
  render() {
    let { routes } = this.props;
    let {
      flag,
      isModalVisible,
      list,
      searchflag,
      searchval,
      LoginisModalVisible,
      form,
      headrightFlag,
    } = this.state;
    return (
      <div className={Indexmodule.index}>
        <header>
          <div className={Indexmodule.indexhead}>
            <div className={Indexmodule.nav_left}>
              {/* loading */}
              <Loading className={Indexmodule.loading}></Loading>
              {/* 切换 */}
              <Menu
                theme={!flag ? 'light' : 'dark'}
                className={Indexmodule.index_manu}
                mode="horizontal"
              >
                {Indexroutes &&
                  Indexroutes.map((item, index) => (
                    <Menu.Item key={index}>
                      <NavLink to={item.path}>
                        <FormattedMessage id={item.meta.title} />
                      </NavLink>
                    </Menu.Item>
                  ))}
              </Menu>
            </div>
            <div className={Indexmodule.nav_right}>
              <select
                id=""
                className={Indexmodule.nav_right_select}
                onChange={this.handSelect.bind(this)}
              >
                <option
                  value="Child"
                  className={Indexmodule.nav_right_select_option}
                >
                  Child
                </option>
                <option value="Engilsh">Engilsh</option>
              </select>

              <p
                className={Indexmodule.nav_right_off}
                onClick={this.handeClickFlag.bind(this)}
              >
                {flag ? (
                  <img src={img2} alt="" className={Indexmodule.indexImg} />
                ) : (
                  <img src={img1} alt="" className={Indexmodule.indexImg} />
                )}
              </p>

              <Icon
                name="search"
                className={Indexmodule.nav_right_search}
                onClick={this.handeClickSearch.bind(this)}
              />

              <button
                className={Indexmodule.nav_rigint_login}
                onClick={this.handeLogin.bind(this)}
              >
                登录
              </button>

              {headrightFlag ? (
                <span
                  className={Indexmodule.xiang}
                  onClick={this.handX.bind(this)}
                >
                  三
                </span>
              ) : (
                <span
                  onClick={this.handX.bind(this)}
                  className={Indexmodule.xiang}
                >
                  X
                </span>
              )}

              {headrightFlag ? (
                <div className={Indexmodule.dis_xy}>
                  <Menu
                    theme={!flag ? 'light' : 'dark'}
                    className={Indexmodule.index_manu}
                    mode="horizontal"
                  >
                    {Indexroutes &&
                      Indexroutes.map((item, index) => (
                        <Menu.Item key={index}>
                          <NavLink to={item.path}>
                            <FormattedMessage id={item.meta.title} />
                          </NavLink>
                        </Menu.Item>
                      ))}
                  </Menu>
                </div>
              ) : (
                ''
              )}
            </div>
          </div>
        </header>

        <section className={Indexmodule.section}>
          <RouterView routes={routes} flag={flag}></RouterView>
        </section>
        {/* 搜索 */}
        <Modal
          title="文章搜索"
          footer={false}
          visible={isModalVisible}
          onCancel={this.handleCancel.bind(this)}
        >
          <p className={Indexmodule.index_medalP}>
            <Input
              placeholder="输入关键字,搜索文章"
              className={Indexmodule.index_SearchInput}
              onChange={this.handeSearchInput.bind(this)}
            />
            <Icon
              name="search"
              className={Indexmodule.index_SearchIcon}
              onClick={this.handeSearchSou.bind(this, searchval)}
            />
          </p>
          {searchflag ? (
            <div
              className={
                searchflag ? Indexmodule.SearchData1 : Indexmodule.SearchData
              }
            >
              {list &&
                list.map((item, index) => {
                  return (
                    <li key={item.id}>
                      <span>{item.publishAt.slice(5, 10)}</span>&emsp;
                      <b>{item.title}</b>
                    </li>
                  );
                })}
            </div>
          ) : (
            '暂无数据'
          )}
        </Modal>
        {/* 登录 */}
        <Modal
          title="账户登录"
          footer={false}
          className={Indexmodule.login}
          visible={LoginisModalVisible}
          onCancel={this.handleCancels.bind(this)}
        >
          <Form
            onFinish={this.onFinish.bind(this)}
            form={form}
            footer={
              <div style={{ margin: '16px 16px 0' }}>
                <Button type="primary" block>
                  提交
                </Button>
              </div>
            }
          >
            <Form.Item name="username" label="名称">
              <Field />
            </Form.Item>
            <Form.Item name="password" label="密码">
              <Field />
            </Form.Item>
          </Form>
          <Divider />
          <button type="submit" onClick={this.handeGitee_Login.bind(this)}>
            第三登录
          </button>
        </Modal>
      </div>
    );
  }
}
export default Index;
