/* eslint-disable react/jsx-pascal-case */
import React, { Component } from 'react';
import Containaer_right from '@/component/Containaer_right';
import Pigemodule from '@/views/style/Pigeonhole.module.less';
import { getReact_detail } from '@/api/GuiDang';
import DetaleLess from '../style/Knowledge.module.less';

// import Containaer_right from '@/component/Containaer_right';
// import Pigemodule from '@/views/style/Pigeonhole.module.less';

export default class Detail extends Component {
  componentDidMount() {
    const getData = async () => {
      const res = await getReact_detail();
      this.setState({
        list: res.data,
      });
    };
    getData();
  }
  render() {
    let { flag } = this.props;
    let { state } = this.props.location;
    return (
      <div className="container_frame">
        <div className={flag ? 'container_left1' : 'container_left'}>
          <div className={Pigemodule.containerTop}>
            <h1>{state.title}</h1>
            <img src={state.cover} alt="" className={DetaleLess.images} />
            <h1>{state.title}</h1>
            <p>{state.summary}</p>
            <div
              dangerouslySetInnerHTML={{ __html: state.html }}
              className={DetaleLess.detale_html}
            ></div>
          </div>
        </div>

        <Containaer_right></Containaer_right>
      </div>
    );
  }
}
