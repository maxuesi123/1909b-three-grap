/* eslint-disable react/jsx-pascal-case */
import React, { Component } from 'react';

import DetaleLess from '../../style/Knowledge.module.less';

import Containaer_right from '@/component/Containaer_right';
import Pigemodule from '../../style/Pigeonhole.module.less';
export default class KnowledgeDetale extends Component {
  render() {
    let { state } = this.props.location;

    let { flag } = this.props;

    return (
      <div className="container_frame">
        <div className={flag ? 'container_left1' : 'container_left'}>
          <div className={Pigemodule.containerTop}>
            <div className={DetaleLess.detale_div_1}>
              <img src={state.cover} alt="" className={DetaleLess.images} />
              <h1>{state.title}</h1>
              <p>{state.summary}</p>
              <div
                dangerouslySetInnerHTML={{ __html: state.html }}
                className={DetaleLess.detale_html}
              ></div>
            </div>
          </div>
        </div>
        <Containaer_right flag={flag}></Containaer_right>
      </div>
    );
  }
}
