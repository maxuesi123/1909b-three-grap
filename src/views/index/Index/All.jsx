// @ts-nocheck
import React, { Component } from 'react';
// @ts-ignore
// import { Readdata } from '../../../api/GuiDang';
import KnoLess from '../../style/All.module.less';
import { Modal, BackTop } from 'antd';
import { QRCodeSVG } from 'qrcode.react';
import axios from 'axios';
import { Icon } from 'react-vant';
const style = {
  height: 40,
  width: 40,
  lineHeight: '40px',
  borderRadius: 4,
  backgroundColor: '#1088e9',
  color: '#fff',
  textAlign: 'center',
  fontSize: 14,
};
class All extends Component {
  state = {
    arr: [],
  };
  componentDidMount() {
    axios.get('https://creationapi.shbwyz.com/api/article').then((res) => {
      this.setState({
        arr: res.data.data[0],
      });
    });
    // const getknoewledges = async () => {
    //   const res = await Readdata();
    //   this.setState({
    //     arr: res.data[0],
    //   });
    // };
    // getknoewledges();
  }
  enjoy() {
    this.setState({
      isModalVisible: true,
    });
  }
  handleOk() {
    this.setState({
      isModalVisible: false,
    });
  }
  handleCancel() {
    this.setState({
      isModalVisible: false,
    });
  }
  render() {
    let { arr } = this.state;
    return (
      <div id="bigbox">
        <div className="container_left">
          {arr &&
            arr.map((item, index) => {
              return (
                <div key={index} className={KnoLess.left_div}>
                  <h4>
                    {item.title}&emsp;&emsp;
                    {item.views}天前
                  </h4>
                  <br />
                  <span>{item.summary}</span>
                  <img src={item.cover} alt="" className={KnoLess.imgs} />
                  <p className={KnoLess.p1}>
                    <br />
                    <span>{item.views}</span>
                    <span onClick={this.enjoy.bind(this)}>分享</span>
                    <Modal
                      title="Basic Modal"
                      visible={this.state.isModalVisible}
                      onOk={this.handleOk.bind(this)}
                      onCancel={this.handleCancel.bind(this)}
                    >
                      <p>核稿人好多女等会佛教凭什么数据</p>
                      <p>核稿人好多女等会佛教凭什么数据</p>
                      <p>核稿人好多女等会佛教凭什么数据</p>
                      <QRCodeSVG value="https://reactjs.org/" />,
                    </Modal>
                  </p>
                </div>
              );
            })}
        </div>
        <div
          style={{
            height: '30vh',
            padding: 8,
          }}
        >
          <BackTop>
            <div style={style}>
              <Icon name="back-top" />
            </div>
          </BackTop>
        </div>
      </div>
    );
  }
}

export default All;
