/* eslint-disable react/jsx-pascal-case */
import React, { Component } from 'react';
// @ts-ignore
import { getGuiDang } from '@/api/GuiDang';
// @ts-ignore
import Pigemodule from '@/views/style/Pigeonhole.module.less';
// @ts-ignore
import Containaer_right from '@/component/Containaer_right';
export class pigeonhole extends Component {
  componentDidMount() {
    const getData = async () => {
      const res = await getGuiDang();

      this.setState({
        list: res.data[2022].May,
      });
    };
    getData();
  }
  state = {
    list: [],
  };
  Todetail(item) {
    this.props.history.push('/detail', item);
  }
  render() {
    const { list } = this.state;
    return (
      <div className="container_frame">
        <div className="container_left">
          <div className={Pigemodule.containerTop}>
            <h1>归档</h1>
            <p>
              共计<span className={Pigemodule.Fontsize}>12</span>篇
            </p>
            <h2>2022</h2>
            <h3>May</h3>
            {list &&
              // @ts-ignore
              list.map((item, index) => {
                return (
                  <li key={item.id} onClick={this.Todetail.bind(this, item)}>
                    <span>{item.publishAt.slice(5, 10)}</span>&emsp;
                    <b>{item.title}</b>
                  </li>
                );
              })}
          </div>
        </div>
        <Containaer_right></Containaer_right>
      </div>
    );
  }
}

export default pigeonhole;
