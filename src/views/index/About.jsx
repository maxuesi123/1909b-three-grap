import React, { Component } from 'react';
import Aboutmodule from '../style/About.module.less';
import Comments from '../../component/comment/index';
// import RViewerJS from 'viewerjs-react';
export class About extends Component {
  render() {
    return (
      <div className={Aboutmodule.about}>
        <div className={Aboutmodule.aboutchildern}>
          {/* 关于的图片 */}
          {/* <RViewerJS> */}
          <img
            src="https://wipi.oss-cn-shanghai.aliyuncs.com/2020-04-04/despaired-2261021_1280.jpg"
            alt="文章封面"
          />
          {/* </RViewerJS> */}
          <div className={Aboutmodule.Aboutupdata}>
            <h1>
              这世界只有一种英雄主义，就是在看清了生活的真相之后，依然热爱生活。
            </h1>
            <div className={Aboutmodule.aboutupdataChildren}>
              <p>
                <b>更新</b>
              </p>
              <ul>
                <li>
                  更新 Github OAuth 登录，由于网络的原因, 容易失败，
                  可以多尝试几下。
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className={Aboutmodule.aboutCommit}>
          <div>
            <Comments></Comments>
          </div>
        </div>
      </div>
    );
  }
}

export default About;
