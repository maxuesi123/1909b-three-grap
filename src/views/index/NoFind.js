import React, { Component } from 'react';
import { Result, Button } from 'antd';
import { withRouter } from 'react-router-dom';
class NoFind extends Component {
  state = {
    time: 5,
  };
  //   time = time+1; time++   time+1
  componentDidMount() {
    this.timer = setInterval(() => {
      let { time } = this.state;
      this.setState(
        {
          time: time - 1,
        },
        () => {
          if (time <= 1) {
            clearInterval(this.timer);
            //回到首页
            // this.props.history.push('/');
            this.props.history.replace('/');
          }
        }
      );
    }, 1000);
  }
  componentWillUnmount() {
    clearInterval(this.timer);
  }
  handleBack = () => {
    clearInterval(this.timer);
    this.props.history.replace('/');
  };
  render() {
    let { time } = this.state;
    return (
      <div>
        <Result
          status="404"
          title="404"
          subTitle="Sorry, the page you visited does not exist."
          extra={
            <Button type="primary" onClick={this.handleBack}>
              {time}秒之后，自动回到首页
            </Button>
          }
        />
      </div>
    );
  }
}
export default withRouter(NoFind);
