import React, { Component } from 'react';
import DetaleLess from '../../style/Knowledge.module.less';

export class Detale extends Component {
  render() {
    let { state } = this.props.location;
    return (
      <div className={DetaleLess.detale_wrap}>
        <div className={DetaleLess.detale_wrap_3}>
          <img src={state.cover} alt="" className={DetaleLess.images} />
        </div>
        <h1>{state.title}</h1>
        <p>{state.summary}</p>
        {/* <p>{state.content}</p> */}
        <div
          dangerouslySetInnerHTML={{ __html: state.html }}
          className={DetaleLess.detale_html}
        ></div>
      </div>
    );
  }
}

export default Detale;
