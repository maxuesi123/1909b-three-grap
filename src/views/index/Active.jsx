/* eslint-disable react/jsx-pascal-case */
import React, { Component } from 'react';
import Containaer_right from '../../component/Containaer_right';
import Activeless from '../style/Active.module.less';
import { NavLink } from 'react-router-dom';
// import { BackTop } from 'antd';
import RouterView from '../../router/RouterView';
// import { Icon } from 'react-vant';
import Top from '../../component/Top';

export class Home extends Component {
  del() {
    window.location.href = '/some';
  }

  render() {
    return (
      <div className="container_frame">
        <div className="container_left">
          <div className={Activeless.img} onClick={this.del.bind(this)}>
            <img
              src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-12-11/IMG_031434567.jpeg"
              alt=""
            />
            <h2>微信小程序首屏性能优化</h2>
            <p>大约2个月前.3770次阅读</p>
          </div>
          <footer>
            <NavLink to="/active/all">所有</NavLink>
            <NavLink to="/active/frontend">前端</NavLink>
            <NavLink to="/active/backend">后端</NavLink>
            <NavLink to="/active/read">阅读</NavLink>
            <NavLink to="/active/linux">Linux</NavLink>
            <NavLink to="/active/leetcode">LeetCode</NavLink>
            <NavLink to="/active/focusnews">要闻</NavLink>
          </footer>
          <main className={Activeless.activeMain}>
            <RouterView routes={this.props.routes}></RouterView>
          </main>
        </div>
        <Top></Top>
        <Containaer_right></Containaer_right>
      </div>
    );
  }
}

export default Home;
