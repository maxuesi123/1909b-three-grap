import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { getKnow_detail } from '@/api/GuiDang';

import KnowLess from '../../style/Knowledge.module.less';

class Knowledge_Right_botm extends Component {
  state = {
    Right_List_1: [],
  };
  componentDidMount() {
    const getKnew_Datail = async () => {
      const res = await getKnow_detail();
      this.setState({
        Right_List_1: res.data,
      });
    };
    getKnew_Datail();
  }
  handDetail(item) {
    this.props.history.push('/detale_List', item);
  }
  render() {
    let { Right_List_1 } = this.state;
    // console.log(Right_List_1);
    return (
      <>
        {
          <>
            <h3 className={KnowLess.Right_List_h3}>文章分类</h3>
            {Right_List_1 &&
              Right_List_1.map((item, index) => {
                return (
                  <div className={KnowLess.Right_List_header} key={index}>
                    <div
                      onClick={this.handDetail.bind(this, item)}
                      className={KnowLess.Right_List_title}
                    >
                      {item.label}
                    </div>

                    <div className={KnowLess.Right_List_views}>
                      共计{item.articleCount}篇文章
                    </div>
                  </div>
                );
              })}
          </>
        }
      </>
    );
  }
}

export default withRouter(Knowledge_Right_botm);
