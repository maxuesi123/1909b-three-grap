/* eslint-disable react/jsx-pascal-case */
/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from 'react';
import Containaer_right from '@/component/Containaer_right';
import KnoLess from '../../style/Knowledge.module.less';
import { getKnow } from '@/api/GuiDang';
import { Icon } from 'react-vant';
import { Modal } from 'antd';
import { QRCodeSVG } from 'qrcode.react';
export class Knowledge extends Component {
  state = {
    arr: [],
    isModalVisible: false,
    list: [],
    Lazyload: true,
  };
  enjoy(item) {
    // console.log(item);
    this.setState({
      list: [item],
      isModalVisible: true,
    });
  }
  handleOk() {
    this.setState({
      isModalVisible: false,
    });
  }
  handleCancel() {
    this.setState({
      isModalVisible: false,
    });
  }
  componentDidMount() {
    const getknoewledge = async () => {
      const res = await getKnow();

      this.setState({
        arr: res.data[0],
      });
    };
    getknoewledge();
  }
  Detail(item) {
    // console.log(item);
    this.props.history.push('/detale', item);
  }

  render() {
    let { arr, list } = this.state;
    // console.log(list);
    let { flag } = this.props;
    return (
      <div className="container_frame">
        <div className={flag ? 'container_left1' : 'container_left'}>
          {arr &&
            arr.map((item, index) => {
              return (
                <div key={index} className={KnoLess.left_div}>
                  <div className={KnoLess.div_wrap}>
                    <div className={KnoLess.title_div}>
                      <h3 onClick={this.Detail.bind(this, item)}>
                        {item.title}
                      </h3>
                    </div>
                    <div className={KnoLess.views_div}>{item.views}天前</div>
                  </div>
                  <p>{item.summary}</p>
                  {/* <img src={item.cover} alt="" className={KnoLess.imgs} /> */}
                  <img
                    className={KnoLess.imgs}
                    lazyload="true"
                    src={item.cover}
                  />
                  <p className={KnoLess.p1}>
                    <br />
                    <Icon name="eye-o" />
                    &emsp;
                    <span>{item.views}</span>&ensp;
                    <Icon name="share-o" />
                    &emsp;
                    <span onClick={this.enjoy.bind(this, item)}>分享</span>
                  </p>
                </div>
              );
            })}

          <Modal
            title="分享海报"
            visible={this.state.isModalVisible}
            onOk={this.handleOk.bind(this)}
            onCancel={this.handleCancel.bind(this)}
          >
            {list &&
              list.map((item1, index1) => {
                return (
                  <div key={index1}>
                    <img src={item1.cover} alt="" className={KnoLess.banner} />
                    <p>{item1.title}</p>
                    <p>{item1.summary}</p>
                    {/* <p>Some contents...</p> */}
                    <QRCodeSVG value="https://reactjs.org/" />
                  </div>
                );
              })}
          </Modal>
        </div>

        <Containaer_right></Containaer_right>
      </div>
    );
  }
}

export default Knowledge;
