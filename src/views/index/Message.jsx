import React, { Component } from 'react';
import style from '../style/message.module.less';
// import Containaer_right from '@/component/Containaer_right';

import { getKnow } from '@/api/GuiDang';
export class Message extends Component {
  state = {
    arr: [],
  };
  componentDidMount() {
    const getknoewledge = async () => {
      const res = await getKnow();
      this.setState({
        arr: res.data[0],
      });
    };
    getknoewledge();
  }
  Detail(item) {
    this.props.history.push('/detale', item);
  }
  render() {
    let { arr } = this.state;
    let { flag } = this.props;
    return (
      <div className={style.messagebox}>
        <div>
          <div className={style.message_head}>
            <h2>留言板</h2>
            <h3>「请勿灌水」</h3>
            <h3>「垃圾评论过多，欢迎提供好的过滤(标记)算法」</h3>
          </div>
          <div className={style.message_bottom}>
            <h3>评论</h3>
            <div className={style.message_bottom_one}></div>
            <h3>阅读</h3>
            <div className={style.message_bottom_two}>
              <div></div>
              <div></div>
            </div>
          </div>
        </div>
        <div className="container_frame">
          <div className={flag ? 'container_left1' : 'container_left'}>
            {arr &&
              arr.map((item, index) => {
                return (
                  <div
                    key={index}
                    className={style.left_div}
                    onClick={this.Detail.bind(this, item)}
                  >
                    <h4>{item.title}</h4>
                    <span>{item.views}天前</span>
                    <p>{item.summary}</p>
                    <img src={item.cover} alt="" className={style.imgs} />
                    <p className={style.p1}>
                      <br />
                      <span>{item.views}</span>&ensp;<span>分享</span>
                    </p>
                  </div>
                );
              })}
          </div>
        </div>
      </div>
    );
  }
}

export default Message;
