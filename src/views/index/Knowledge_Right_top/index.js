import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { getReact_detail } from '@/api/GuiDang';
import KnowLess from '../../style/Knowledge.module.less';

export class Knowledge_Right extends Component {
  state = {
    Right_List: [],
  };
  componentDidMount() {
    const getReact_Detail = async () => {
      const res = await getReact_detail();
      this.setState({
        Right_List: res.data,
      });
    };
    getReact_Detail();
  }
  handDetail(item) {
    this.props.history.push('/knowledgedetale', item);
  }
  render() {
    let { Right_List } = this.state;
    return (
      <>
        <h3 className={KnowLess.Right_List_h3}>推荐阅读</h3>
        {Right_List &&
          Right_List.map((item, index) => {
            return (
              <div className={KnowLess.Right_List_header} key={index}>
                <div
                  onClick={this.handDetail.bind(this, item)}
                  className={KnowLess.Right_List_title}
                >
                  {item.title}
                </div>
                <div className={KnowLess.Right_List_views}>
                  · &emsp; 大约{item.views}个月前
                </div>
              </div>
            );
          })}
      </>
    );
  }
}

export default withRouter(Knowledge_Right);
