/* eslint-disable react/jsx-pascal-case */
import React, { Component } from 'react';
import DetaleLess from '../../style/Knowledge.module.less';

import Containaer_right from '@/component/Containaer_right';
import Pigemodule from '@/views/style/Pigeonhole.module.less';
import { NavLink } from 'react-router-dom';

export default class KnowledgeDetale extends Component {
  render() {
    let { state } = this.props.location;
    // console.log(state);
    let { flag } = this.props;
    return (
      <div className="container_frame">
        <div className={flag ? 'container_left1' : 'container_left'}>
          <div className={Pigemodule.containerTop}>
            <h1>
              {state.label}
              <span className={DetaleLess.sp1}> 分类文章</span>
            </h1>
            <div className={DetaleLess.detale_div}>
              <h3>共搜到{state.articleCount}篇</h3>
            </div>
          </div>
          <footer>
            <NavLink to="/active/frontend">前端</NavLink>
            <NavLink to="/active/backend">后端</NavLink>
            <NavLink to="/active/read">阅读</NavLink>
            <NavLink to="/active/linux">Linux</NavLink>
            <NavLink to="/active/leetcode">LeetCode</NavLink>
            <NavLink to="/active/focusnews">要闻</NavLink>
          </footer>
        </div>

        <Containaer_right flag={flag}></Containaer_right>
      </div>
    );
  }
}
