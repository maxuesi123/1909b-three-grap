import React, { Component } from 'react';
import Containaer_right from '../../component/Containaer_right';
import Activ from '../style/Active.module.less';
import { floorData } from '@/api/GuiDang';
import { Modal } from 'antd';
import { QRCodeSVG } from 'qrcode.react';
// import RViewerJS from 'viewerjs-react';
import { Icon } from 'react-vant';
import Top from '@/component/Top';

export class Some extends Component {
  state = {
    list: [],
    isModalVisible: false,
    count: 1,
  };
  xin = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };
  enjoy() {
    this.setState({
      isModalVisible: true,
    });
  }
  handleOk() {
    this.setState({
      isModalVisible: true,
    });
    // message.handleOk('请下载')
  }
  handleCancel() {
    this.setState({
      isModalVisible: false,
    });
  }

  componentDidMount() {
    const Datalist = async () => {
      const res = await floorData();
      this.setState({
        list: res.data[2022].May,
      });
    };
    Datalist();
  }
  render() {
    let { flag } = this.props;
    let { list, count } = this.state;
    return (
      <div className="container_frame">
        <div className={flag ? 'container_left1' : 'container_left'}>
          <div className={Activ.some}>
            <div className={Activ.bigimg}>
              {/* <RViewerJS> */}
              <img
                src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-12-11/IMG_031434567.jpeg"
                alt=""
              />
              {/* </RViewerJS> */}
              <h1>微信小程序首屏性能优化</h1>
              <p>发布于2022-04-02 17:01:35.阅读量 3786</p>
            </div>
            <div className={Activ.sum}>
              <Icon name="like" onClick={this.xin} />
              <span>{count}</span>
            </div>
            <div className={Activ.floor}>
              <p onClick={this.enjoy.bind(this)}>
                <Icon name="cluster-o" />
              </p>
              <Modal
                title="分享海报"
                okText="下载"
                cancelText="取消"
                visible={this.state.isModalVisible}
                onOk={this.handleOk.bind(this)}
                onCancel={this.handleCancel.bind(this)}
              >
                <div className={Activ.modal}>
                  <img
                    src="https://wipi.oss-cn-shanghai.aliyuncs.com/2020-04-04/despaired-2261021_1280.jpg"
                    alt=""
                  />
                  <p>核稿人好多女等会佛教凭什么数据</p>
                  <QRCodeSVG value="https://reactjs.org/" />,
                </div>
              </Modal>
              <div className={Activ.left}>
                <div></div>
                {list &&
                  list.map((item, index) => {
                    return (
                      <div key={item.id}>
                        <span>{item.publishAt.slice(5, 10)}</span>&emsp;
                        <b>{item.title}</b>
                        <br />
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        </div>
        <Top />
        <Containaer_right flag={flag}></Containaer_right>
        {/* <div className={Activ.right}></div> */}
      </div>
    );
  }
}

export default Some;
