import React, { Component } from 'react';
import './styles/common.less';
import { BrowserRouter } from 'react-router-dom';
import routes from './router/routerConfig';
import RouterView from './router/RouterView';
import './App.css';
import { IntlProvider } from 'react-intl';
import { inject, observer } from 'mobx-react';

@inject('titlestore')
@observer
export class App extends Component {
  render() {
    const { message, lange } = this.props.titlestore;
    return (
      <div className="container">
        <IntlProvider messages={message} locale={lange} defaultLocale="en">
          <BrowserRouter>
            <RouterView routes={routes}></RouterView>
          </BrowserRouter>
        </IntlProvider>
      </div>
    );
  }
}

export default App;
