import { lazy } from 'react';

export const Indexroutes = [
  {
    path: '/active',
    meta: {
      title: 'active',
    },
    component: lazy(() => import('../views/index/Active')),
  },
  {
    path: '/pigeonhole',
    meta: {
      title: 'pigeonhole',
    },
    component: lazy(() => import('../views/index/Pigeonhole')),
  },
  {
    path: '/knowledge',
    meta: {
      title: 'knowledge',
    },
    component: lazy(() => import('../views/index/Knowledge/Knowledge')),
  },
  {
    path: '/message',
    meta: {
      title: 'message',
    },
    component: lazy(() => import('../views/index/Message')),
  },
  {
    path: '/about',
    meta: {
      title: 'about',
    },
    component: lazy(() => import('../views/index/About')),
  },
];

export const IndexroutesChilde = [
  {
    path: '/active',
    meta: {
      title: ' 文章 ',
    },
    component: lazy(() => import('../views/index/Active')),
    children: [
      // {
      //   path: '/active',
      //   redirect: '/active/frontend',
      // },
      {
        path: '/active/all',
        component: lazy(() => import('../views/index/Index/All')),
      },
      {
        path: '/active/frontend',
        component: lazy(() => import('../views/index/Index/FrontEnd')),
      },
      {
        path: '/active/backend',
        component: lazy(() => import('../views/index/Index/BackEnd')),
      },
      {
        path: '/active/read',
        component: lazy(() => import('../views/index/Index/Read')),
      },
      {
        path: '/active/linux',
        component: lazy(() => import('../views/index/Index/Linux')),
      },
      {
        path: '/active/leetcode',
        component: lazy(() => import('../views/index/Index/LeetCode')),
      },
      {
        path: '/active/focusnews',
        component: lazy(() => import('../views/index/Index/FocusNews')),
      },
      {
        path: '/active',
        redirect: '/active/all',
      },
    ],
  },
  {
    path: '/pigeonhole',
    meta: {
      title: ' 归档 ',
    },
    component: lazy(() => import('../views/index/Pigeonhole')),
  },
  {
    path: '/knowledge',
    meta: {
      title: ' 知识小册 ',
    },
    component: lazy(() => import('../views/index/Knowledge/Knowledge')),
  },
  {
    path: '/message',
    meta: {
      title: ' 留言板 ',
    },
    component: lazy(() => import('../views/index/Message')),
  },
  {
    path: '/about',
    meta: {
      title: ' 关于 ',
    },
    component: lazy(() => import('../views/index/About')),
  },
];
const routes = [
  {
    path: '/',
    component: lazy(() => import('../views/Index')),
    children: [
      // {
      //   path: '*',
      //   component: lazy(() => import('@/views/index/NoFind')),
      // },
      ...IndexroutesChilde,

      {
        path: '/detail',
        component: lazy(() => import('../views/index/Detail')),
      },

      {
        path: '/knowledgedetale',
        component: lazy(() =>
          import('../views/index/KnowledgeDetale/KnowledgeDetale')
        ),
      },
      {
        path: '/detale_List',
        component: lazy(() => import('../views/index/Detale_List')),
      },
      {
        path: '/detale',
        component: lazy(() => import('../views/index/Detale')),
      },
      {
        path: '/some',
        component: lazy(() => import('../views/index/Some')),
      },
      {
        path: '/',
        redirect: '/active', // 重定向:重新指向其它path,会改变网址
      },
      {
        path: '*',

        component: lazy(() => import('@/views/index/NoFind')),
      },
    ],
  },
];
export default routes;
