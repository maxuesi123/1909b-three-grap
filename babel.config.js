const plugins = [];
if (process.env.NODE_ENV === 'production') {
  //只有开发环境的时候，才来移除console
  plugins.push('transform-remove-console');
}
module.exports = {
  presets: ['react-app', '@babel/preset-env'],
  plugins: [
    [
      '@babel/plugin-proposal-decorators',
      {
        legacy: true,
      },
    ],
  ],
};
